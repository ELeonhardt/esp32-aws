#include <Arduino.h>

#include "secrets.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"

// The MQTT topics that this device should publish/subscribe
#define AWS_IOT_PUBLISH_TOPIC "esp32/pub"
#define AWS_IOT_SUBSCRIBE_TOPIC "esp32/sub"
const String AWS_IOT_PREFIX = "$aws/things/esp32/shadow/name/test-shaddow";

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);
void messageHandler(String &topic, String &payload);
int sent = 0;

void connectAWS()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  // Configure WiFiClientSecure to use the AWS IoT device credentials
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);

  // Connect to the MQTT broker on the AWS endpoint we defined earlier
  client.begin(AWS_IOT_ENDPOINT, 8883, net);
  //test
  // Create a message handler
  client.onMessage(messageHandler);

  Serial.print("Connecting to AWS IOT");

  while (!client.connect(THINGNAME))
  {
    Serial.print(".");
    delay(100);
  }

  if (!client.connected())
  {
    Serial.println("AWS IoT Timeout!");
    return;
  }

  // Subscribe to a topic
  bool subscribed = client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);
  Serial.println("subscribed to esp32/sub: " + subscribed);
  const char *shadow = "$aws/things/esp32/shadow/name/test-shaddow/get/accepted";
  //subscribed = client.subscribe(shadow);
  //Serial.println("subscribed to shaddow/get/accepted: " + subscribed);
  // client.subscribe(AWS_IOT_PREFIX + "/delete/accepted");
  // client.subscribe(AWS_IOT_PREFIX + "/delete/rejected");
  // client.subscribe(AWS_IOT_PREFIX + "/get/accepted");
  // client.subscribe(AWS_IOT_PREFIX + "/get/rejected");
  // client.subscribe(AWS_IOT_PREFIX + "/update/accepted");
  // client.subscribe(AWS_IOT_PREFIX + "/update/rejected");
  // client.subscribe(AWS_IOT_PREFIX + "/update/delta");
  // client.subscribe(AWS_IOT_PREFIX + "/update/documents");

  Serial.println("AWS IoT Connected!");
}

void publishMessage()
{
  StaticJsonDocument<500> doc;
  doc["board_cableset"] = "05TEC4439";
  doc["timestamp"] = String(millis(), DEC);
  doc["board"] = "TEC05";
  doc["cableset"] = "TEC4439";
  doc["test_result"] = "OK";
  doc["id"] = "4567";
  doc["error"] = "bad pin to pin connection.";
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); // print to client
  Serial.println(jsonBuffer);

  bool published = client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
  //published = client.publish("$aws/things/esp32/shadow/name/test-shaddow/get", "");
  if (published)
  {
    Serial.println("message published successfully.");
  }
  else
  {
    Serial.println("error publishing the message to AWS.");
  }
  //client.publish(AWS_IOT_PREFIX+"/get");
}

void messageHandler(String &topic, String &payload)
{
  Serial.println("incoming: " + topic + " - " + payload);
  Serial.println("I have received a message");

  //  StaticJsonDocument<200> doc;
  //  deserializeJson(doc, payload);
  //  const char* message = doc["message"];
}

void setup()
{
  Serial.begin(9600);
  connectAWS();
}

void loop()
{
  if (sent < 5)
  {
    Serial.println("message sent");
    publishMessage();
    sent++;
  }
  else
  {
    Serial.println("message already sent, not sending again");
  }
  delay(5000);
  client.loop();
}